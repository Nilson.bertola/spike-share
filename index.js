const app = require("express")();
const cors = require("cors");

const getProfile = {
  silvio: {
    title: "Silvin Matheus Cauê da Rosa",
    image:
      "https://conteudo.imguol.com.br/c/entretenimento/1d/2017/12/19/silvinho-gala-1513716623078_v2_900x506.jpg",
    url: "www.google.com",
    description:
      "Vivamus nec platea adipiscing scelerisque cras justo nostra varius eleifend mi, non luctus himenaeos et eget eros ad non cursus lobortis, pellentesque dui donec amet integer auctor nisl lorem neque."
  },
  default: {
    title: "Não encontrado",
    image:
      "https://ets2.lt/wp-content/uploads/profile_builder/avatars/userID_24995_originalAvatar_Cool-avatars-anonymous-avatar.jpg?x96452",
    url: "www.google.com",
    description: "Uma descrição qualquer."
  }
};

const renderHtml = (profile, header) => `
<html prefix="og: http://ogp.me/ns#">
<head>
<title>${profile.title}</title>
${
  String(header["user-agent"]).includes("facebook")
    ? ``
    : `<meta property="og:url" content="${profile.url}" />
        <meta http-equiv="refresh" content="1;URL=${profile.url}">`
}
<meta property="og:title" content="${profile.title}" />
<meta property="og:type" content="website" />
<meta property="og:locale" content="pt_BR" />
<meta property="og:description" content="${profile.description}" />
<meta property="og:url" content="${profile.url}" />
<meta property="og:image" content="${profile.image}" />
<meta property="og:image:height" content="900" />
<meta property="og:image:width" content="500" />
<meta http-equiv="refresh" content="1;URL=${profile.url}">
</head>
${
  String(header["user-agent"]).includes("facebook")
    ? `<script>
window.onload = function() {
  window.location.href="${profile.url}";
}
</script>`
    : ``
}
</html>
`;

app.use(cors());

app.get("/compartilhar/:user", (request, response) => {
  const { user } = request.params;
  console.log(request.headers);
  response.send(
    renderHtml(getProfile[user] || getProfile.default, request.headers)
  );
  response.redirect('/')
});

app.all("*", (_, req) => req.send("Please GET on /compartilhar/:user ."));

app.listen(process.env.PORT || 3000, function() {
  console.log(
    "Express server listening on port %d in %s mode",
    this.address().port,
    app.settings.env
  );
});
